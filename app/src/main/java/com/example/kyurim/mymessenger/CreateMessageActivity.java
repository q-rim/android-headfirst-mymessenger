package com.example.kyurim.mymessenger;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.EditText;

public class CreateMessageActivity extends AppCompatActivity {

    private static final String TAG = "----CreateMsgActivity: ";
    private String userMessage;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        Log.i(TAG, "onCreate()");
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_create_message);
    }

    public void onClickSendMessage(View view) {
        Log.i(TAG, "Button Clicked");

        // EditText - user message prompt
        EditText messageView = (EditText) findViewById(R.id.message_EditText);
        userMessage = messageView.getText().toString();                     // get user message from EditText
        Log.i(TAG, "onClickSendMessage(): " + userMessage);

        // Intent - Activity-to-Activity in a different App
        Intent intent = new Intent(Intent.ACTION_SEND);     // Create Implicit Intent Object for 2nd Activity
        intent.setType("text/plain");                       // specify the intent type
        intent.putExtra(Intent.EXTRA_TEXT, userMessage);    // sending value to another Activity

        // Activity chooser - disable "selection; JUST ONCE / ALWAYS" option
        String chooserTitle = getString(R.string.activity_chooser);
        Intent chosenIntent = Intent.createChooser(intent, chooserTitle);       // launch Activity chooser
        startActivity(chosenIntent);                                            // start 2nd Activity
    }
}
