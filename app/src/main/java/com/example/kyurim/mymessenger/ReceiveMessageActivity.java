package com.example.kyurim.mymessenger;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.widget.TextView;

public class ReceiveMessageActivity extends Activity {

    private static final String TAG = "----RxMsgActivity: ";
    public static final String KEY = "KEY_RECEIVE_MESSAGE_ACTIVITY";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        Log.i(TAG, "onCreate()");
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_receive_message);

        // Receiving values from another Activity
        Intent intent = getIntent();                                // create Intent for receiving
        String messageValue = intent.getStringExtra(KEY);        // get value from another Activity
        Log.i(TAG, messageValue);

        // TextView - display user message
        TextView userMessage = (TextView) findViewById(R.id.message_TextView);     // Create TextView
        userMessage.setText(messageValue);                   // print user message on textView
    }
}
